module Api.Connecting exposing (ServerToClient(..))

import Api.Room



-- DATA STRUCTURES


type ServerToClient
    = JoinLobby String (List String)
    | JoinRoom String Api.Room.Snapshot
