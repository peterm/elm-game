module Api.Room exposing (ServerToClient(..), Snapshot, Tile(..))

import Array exposing (Array)



-- DATA STRUCTURES


type Tile
    = Empty
    | SoftBlock
    | HardBlock


type alias Snapshot =
    { players : List String
    , tiles : Array Tile
    }


type ServerToClient
    = UpdateRoom Snapshot
