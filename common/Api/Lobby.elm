module Api.Lobby exposing (ServerToClient(..))

import Api.Room



-- DATA STRUCTURES


type ServerToClient
    = UpdateLobby (List String)
    | JoinRoom Api.Room.Snapshot
