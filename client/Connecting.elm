module Connecting exposing (Model, Msg, Routing(..), init, subscriptions, update, view)

import Api.Connecting
import Api.Room
import Html exposing (..)
import Json.Decode exposing (..)
import Net
import Room



-- DATA STRUCTURES


type alias Model =
    ()


type alias Msg =
    Result Error Api.Connecting.ServerToClient


type Routing
    = GoToLobby String (List String)
    | GoToRoom String Api.Room.Snapshot
    | Stay



-- INIT


init : Model
init =
    ()



-- UPDATE


update : Msg -> Model -> Routing
update msg model =
    case msg of
        Ok (Api.Connecting.JoinLobby name players) ->
            GoToLobby name players

        Ok (Api.Connecting.JoinRoom name snapshot) ->
            GoToRoom name snapshot

        Err _ ->
            Stay



-- SUBSCRIPTIONS


subscriptions : Model -> Sub Msg
subscriptions model =
    Net.recv decoder


decoder : Decoder Api.Connecting.ServerToClient
decoder =
    oneOf
        [ field "lobby" (map2 Api.Connecting.JoinLobby (field "name" string) (field "players" (list string)))
        , field "room" (map2 Api.Connecting.JoinRoom (field "name" string) (field "snapshot" Room.decodeSnapshot))
        ]



-- VIEW


view : Model -> Html Msg
view model =
    h1 []
        [ text "Connecting" ]
