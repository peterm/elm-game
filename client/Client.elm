module Client exposing (main)

import Browser
import Connecting
import Html exposing (..)
import Lobby
import Net
import Room


main : Program Flags Model Msg
main =
    Browser.element
        { init = init
        , view = view
        , update = update
        , subscriptions = subscriptions
        }



-- DATA STRUCTURES


type alias Flags =
    {}


type Model
    = ConnectingScreen Connecting.Model
    | LobbyScreen Lobby.Model
    | RoomScreen Room.Model


type Msg
    = ConnectingMsg Connecting.Msg
    | LobbyMsg Lobby.Msg
    | RoomMsg Room.Msg



-- INIT


init : Flags -> ( Model, Cmd Msg )
init flags =
    ( ConnectingScreen Connecting.init, Cmd.none )



-- UPDATE


update : Msg -> Model -> ( Model, Cmd Msg )
update msg model =
    case ( msg, model ) of
        ( ConnectingMsg screenMsg, ConnectingScreen screenModel ) ->
            updateConnecting screenMsg screenModel

        ( LobbyMsg screenMsg, LobbyScreen screenModel ) ->
            updateLobby screenMsg screenModel

        ( RoomMsg roomMsg, RoomScreen roomModel ) ->
            updateRoom roomMsg roomModel

        ( _, _ ) ->
            ( model, Cmd.none )


updateConnecting : Connecting.Msg -> Connecting.Model -> ( Model, Cmd Msg )
updateConnecting msg model =
    let
        routing =
            Connecting.update msg model
    in
    case routing of
        Connecting.GoToLobby name players ->
            ( LobbyScreen { name = name, players = players }, Cmd.none )

        Connecting.GoToRoom name snapshot ->
            ( RoomScreen { name = name, players = snapshot.players }, Cmd.none )

        Connecting.Stay ->
            ( ConnectingScreen model, Cmd.none )


updateLobby : Lobby.Msg -> Lobby.Model -> ( Model, Cmd Msg )
updateLobby msg model =
    let
        routing =
            Lobby.update msg model
    in
    case routing of
        Lobby.Stay updatedModel ->
            ( LobbyScreen updatedModel, Cmd.none )

        Lobby.JoinRoom snapshot ->
            ( RoomScreen { name = model.name, players = snapshot.players }, Cmd.none )


updateRoom : Room.Msg -> Room.Model -> ( Model, Cmd Msg )
updateRoom msg model =
    let
        routing =
            Room.update msg model
    in
    case routing of
        Room.Stay updatedModel ->
            ( RoomScreen updatedModel, Cmd.none )



-- SUBSCRIPTIONS


subscriptions : Model -> Sub Msg
subscriptions model =
    case model of
        ConnectingScreen screenModel ->
            Sub.map ConnectingMsg (Connecting.subscriptions screenModel)

        LobbyScreen screenModel ->
            Sub.map LobbyMsg (Lobby.subscriptions screenModel)

        RoomScreen roomModel ->
            Sub.map RoomMsg (Room.subscriptions roomModel)



-- VIEW


view : Model -> Html Msg
view model =
    case model of
        ConnectingScreen screenModel ->
            Html.map ConnectingMsg (Connecting.view screenModel)

        LobbyScreen screenModel ->
            Html.map LobbyMsg (Lobby.view screenModel)

        RoomScreen roomModel ->
            Html.map RoomMsg (Room.view roomModel)
