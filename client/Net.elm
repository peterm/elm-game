port module Net exposing (recv)

import Json.Decode exposing (..)


recv : Decoder msg -> Sub (Result Error msg)
recv decoder =
    recvString (decodeString decoder)


port recvString : (String -> msg) -> Sub msg
