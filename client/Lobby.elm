module Lobby exposing (Model, Msg, Routing(..), subscriptions, update, view)

import Api.Lobby
import Api.Room
import Html exposing (..)
import Json.Decode exposing (..)
import Net
import Room



-- DATA STRUCTURES


type alias Model =
    { name : String
    , players : List String
    }


type alias Msg =
    Result Error Api.Lobby.ServerToClient


type Routing
    = Stay Model
    | JoinRoom Api.Room.Snapshot



-- UPDATE


update : Msg -> Model -> Routing
update msg model =
    case msg of
        Ok (Api.Lobby.UpdateLobby names) ->
            Stay { model | players = names }

        Ok (Api.Lobby.JoinRoom snapshot) ->
            JoinRoom snapshot

        Err _ ->
            Stay model



-- SUBSCRIPTIONS


subscriptions : Model -> Sub Msg
subscriptions model =
    Net.recv decoder


decoder : Decoder Api.Lobby.ServerToClient
decoder =
    Json.Decode.oneOf
        [ Json.Decode.map Api.Lobby.UpdateLobby (field "update" (list string))
        , Json.Decode.map Api.Lobby.JoinRoom (field "room" Room.decodeSnapshot)
        ]



-- VIEW


view : Model -> Html Msg
view model =
    div []
        [ h1 [] [ text "Lobby" ]
        , p [] [ text "Connected players:" ]
        , ul [] (List.map (player model.name) model.players)
        ]


player : String -> String -> Html Msg
player yourName name =
    if name == yourName then
        li [] [ strong [] [ text name ], text " (you)" ]

    else
        li [] [ text name ]
