module Room exposing (Model, Msg, Routing(..), decodeSnapshot, init, subscriptions, update, view)

import Api.Room exposing (..)
import Array exposing (Array)
import Html
import Json.Decode exposing (..)
import Net
import Svg exposing (..)
import Svg.Attributes exposing (..)



-- DATA STRUCTURES


type alias TileRow =
    Array Tile


type alias TileGrid =
    Array TileRow


type alias Model =
    { name : String
    , players : List String
    , tiles : TileGrid
    }


type alias Msg =
    Result Error ServerToClient


type Routing
    = Stay Model



-- INIT


init : String -> Snapshot -> Model
init name snapshot =
    { name = name
    , players = snapshot.players
    , tiles = snapshot.tiles
    }



-- UPDATE


update : Msg -> Model -> Routing
update msg model =
    case msg of
        Ok (UpdateRoom snapshot) ->
            Stay { model | players = snapshot.players }

        Err _ ->
            Stay model



-- SUBSCRIPTIONS


subscriptions : Model -> Sub Msg
subscriptions model =
    Net.recv decoder


decoder : Decoder ServerToClient
decoder =
    Json.Decode.map UpdateRoom (field "update" decodeSnapshot)


decodeSnapshot : Decoder Snapshot
decodeSnapshot =
    Json.Decode.map2 (\players tiles -> { players = players, tiles = tiles }) (field "players" (list Json.Decode.string)) (field "tiles" (array decodeTile))


decodeTile : Decoder Tile
decodeTile =
    Json.Decode.map parseTile Json.Decode.string


parseTile : String -> Tile
parseTile str =
    case
        str
    of
        "s" ->
            SoftBlock

        "h" ->
            HardBlock

        _ ->
            Empty



-- VIEW


view : Model -> Html.Html Msg
view model =
    Html.div []
        [ svg
            [ viewBox "0 0 15 13"
            ]
            [ rect [ x "0", y "0", width "1", height "1" ] []
            , rect [ x "1", y "1", width "1", height "1" ] []
            , rect [ x "2", y "2", width "1", height "1" ] []
            ]
        ]


player : String -> String -> Html.Html Msg
player yourName name =
    if name == yourName then
        Html.li [] [ Html.strong [] [ text name ], text " (you)" ]

    else
        Html.li [] [ text name ]
