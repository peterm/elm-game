const Elm = require('./server.elm.js');
const app = Elm.Elm.Server.init({});

const WebSocket = require('ws');
const wss = new WebSocket.Server({ host: "localhost", port: 8080 });

const clients = {};
var nextClientID = 0;

app.ports.sendString.subscribe(function send(args) {
  const [clientID, message] = args;
  const ws = clients[clientID];
  ws.send(message);
});

wss.on('connection', function connection(ws, req) {
  const clientID = nextClientID++;
  clients[clientID] = ws;
  app.ports.connections.send(clientID);

  ws.on('message', function incoming(message) {
    app.ports.recvString.send([clientID, message]);
  });

  ws.on('close', function closing(code, reason) {
    clients[clientID] = null;
    app.ports.disconnections.send(clientID);
  });
});
