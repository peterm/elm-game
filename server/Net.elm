port module Net exposing (connections, disconnections, recv, send, sendToAll)

import Json.Decode exposing (Decoder, Error, decodeString)
import Json.Encode exposing (Value, encode)


recv : Decoder msg -> Sub ( Int, Result Error msg )
recv decoder =
    recvString (decode decoder)


send : Int -> Value -> Cmd msg
send client msg =
    sendString ( client, encode 0 msg )


sendToAll : List Int -> Value -> Cmd msg
sendToAll clients msg =
    let
        encoded =
            encode 0 msg

        sendToClient client =
            sendString ( client, encoded )
    in
    Cmd.batch (List.map sendToClient clients)


decode : Decoder msg -> ( Int, String ) -> ( Int, Result Error msg )
decode decoder ( client, str ) =
    ( client, decodeString decoder str )



-- PORTS


port connections : (Int -> msg) -> Sub msg


port disconnections : (Int -> msg) -> Sub msg


port recvString : (( Int, String ) -> msg) -> Sub msg


port sendString : ( Int, String ) -> Cmd msg
