module Server exposing (main)

import Dict exposing (Dict)
import Lobby
import Room


main : Program Flags Model Msg
main =
    Platform.worker
        { init = init
        , update = update
        , subscriptions = subscriptions
        }



-- DATA STRUCTURES


type alias Flags =
    {}


type alias Model =
    { lobby : Lobby.Model
    , rooms : Dict Int Room.Model
    , nextRoom : Int
    }


type Msg
    = LobbyMsg Lobby.Msg
    | RoomMsg Int Room.Msg



-- INIT


init : Flags -> ( Model, Cmd Msg )
init flags =
    ( { lobby = Lobby.init
      , rooms = Dict.empty
      , nextRoom = 0
      }
    , Cmd.none
    )



-- UPDATE


update : Msg -> Model -> ( Model, Cmd Msg )
update msg prev =
    case msg of
        LobbyMsg lobbyMsg ->
            updateLobby lobbyMsg prev

        RoomMsg id roomMsg ->
            updateRoom id roomMsg prev


updateLobby : Lobby.Msg -> Model -> ( Model, Cmd Msg )
updateLobby msg prev =
    let
        nextLobby =
            Lobby.update msg prev.lobby

        ( rooms, nextRoom ) =
            case nextLobby.room of
                Just room ->
                    ( Dict.insert prev.nextRoom room prev.rooms, prev.nextRoom + 1 )

                Nothing ->
                    ( prev.rooms, prev.nextRoom )

        model =
            { lobby = nextLobby.model
            , rooms = rooms
            , nextRoom = nextRoom
            }
    in
    ( model, Cmd.map LobbyMsg nextLobby.cmd )


updateRoom : Int -> Room.Msg -> Model -> ( Model, Cmd Msg )
updateRoom id msg prev =
    case Dict.get id prev.rooms of
        Just room ->
            let
                roomUpdate =
                    Room.update msg room
            in
            case roomUpdate of
                Room.Keep ( nextRoom, cmd ) ->
                    ( { prev | rooms = Dict.insert id nextRoom prev.rooms }, Cmd.map (RoomMsg id) cmd )

                Room.Stop ->
                    ( { prev | rooms = Dict.remove id prev.rooms }, Cmd.none )

        Nothing ->
            ( prev, Cmd.none )



-- SUBSCRIPTIONS


subscriptions : Model -> Sub Msg
subscriptions model =
    Sub.batch
        [ Sub.map LobbyMsg (Lobby.subscriptions model.lobby)
        , Sub.batch (roomsSubscriptions model.rooms)
        ]


roomsSubscriptions : Dict Int Room.Model -> List (Sub Msg)
roomsSubscriptions rooms =
    Dict.toList rooms
        |> List.map roomSubscriptions


roomSubscriptions : ( Int, Room.Model ) -> Sub Msg
roomSubscriptions ( id, model ) =
    Sub.map (RoomMsg id) (Room.subscriptions model)
