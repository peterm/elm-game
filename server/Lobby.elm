module Lobby exposing (Model, Msg, Update, init, subscriptions, update)

import Api.Connecting
import Api.Lobby
import Dict exposing (..)
import Json.Encode exposing (..)
import Net
import Room



-- DATA STRUCTURES


type alias Player =
    { name : String }


type alias Model =
    { players : Dict Int Player }


type Msg
    = Connect Int
    | Disconnect Int


type alias Update =
    { model : Model
    , cmd : Cmd Msg
    , room : Maybe Room.Model
    }



-- INIT


init : Model
init =
    { players = Dict.empty }



-- UPDATE


update : Msg -> Model -> Update
update msg prev =
    case msg of
        Connect client ->
            let
                connectingPlayerName =
                    "Player " ++ String.fromInt client

                connectingPlayer =
                    { name = connectingPlayerName }

                players =
                    Dict.insert client connectingPlayer prev.players

                allPlayerNames =
                    playerNames players
            in
            if Dict.size players >= 2 then
                let
                    joinPlayerToRoom =
                        Api.Connecting.JoinRoom connectingPlayerName { players = allPlayerNames }
                            |> encodeConnecting
                            |> Net.send client

                    movePlayersToRoom =
                        Api.Lobby.JoinRoom { players = allPlayerNames }
                            |> encodeLobby
                            |> Net.sendToAll (Dict.keys prev.players)

                    room =
                        players
                            |> Dict.map (\id player -> player.name)
                            |> Room.init
                in
                { model = { players = Dict.empty }
                , cmd = Cmd.batch [ joinPlayerToRoom, movePlayersToRoom ]
                , room = Just room
                }

            else
                let
                    joinPlayerToLobby =
                        Api.Connecting.JoinLobby connectingPlayerName allPlayerNames
                            |> encodeConnecting
                            |> Net.send client

                    updateLobby =
                        Api.Lobby.UpdateLobby allPlayerNames
                            |> encodeLobby
                            |> Net.sendToAll (Dict.keys prev.players)
                in
                { model = { players = players }
                , cmd = Cmd.batch [ joinPlayerToLobby, updateLobby ]
                , room = Nothing
                }

        Disconnect client ->
            let
                otherPlayersInLobby =
                    Dict.remove client prev.players

                updateLobby =
                    playerNames otherPlayersInLobby
                        |> Api.Lobby.UpdateLobby
                        |> encodeLobby
                        |> Net.sendToAll (Dict.keys otherPlayersInLobby)
            in
            { model = { players = otherPlayersInLobby }
            , cmd = updateLobby
            , room = Nothing
            }


playerNames : Dict Int Player -> List String
playerNames players =
    Dict.values players |> List.map (\player -> player.name)


encodeConnecting : Api.Connecting.ServerToClient -> Value
encodeConnecting msg =
    case msg of
        Api.Connecting.JoinLobby name names ->
            object [ ( "lobby", object [ ( "name", string name ), ( "players", list string names ) ] ) ]

        Api.Connecting.JoinRoom name snapshot ->
            object [ ( "room", object [ ( "name", string name ), ( "snapshot", Room.encodeSnapshot snapshot ) ] ) ]


encodeLobby : Api.Lobby.ServerToClient -> Value
encodeLobby msg =
    case msg of
        Api.Lobby.UpdateLobby names ->
            object [ ( "update", list string names ) ]

        Api.Lobby.JoinRoom snapshot ->
            object [ ( "room", Room.encodeSnapshot snapshot ) ]



-- SUBSCRIPTIONS


subscriptions : Model -> Sub Msg
subscriptions clients =
    Sub.batch
        [ Net.connections Connect
        , Net.disconnections Disconnect
        ]
