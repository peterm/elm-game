module Room exposing (Model, Msg, Update(..), encodeSnapshot, init, subscriptions, update)

import Api.Room
import Dict exposing (Dict)
import Json.Encode exposing (..)
import Net



-- DATA STRUCTURES


type alias Player =
    { name : String
    }


type alias Model =
    { players : Dict Int Player
    }


type Msg
    = Disconnect Int


type Update
    = Keep ( Model, Cmd Msg )
    | Stop



-- INIT


init : Dict Int String -> Model
init playerNames =
    { players = Dict.map (\id name -> { name = name }) playerNames
    }



-- UPDATE


update : Msg -> Model -> Update
update msg prev =
    case msg of
        Disconnect client ->
            let
                nextPlayers =
                    Dict.remove client prev.players

                nextModel =
                    { players = nextPlayers }
            in
            if Dict.isEmpty nextPlayers then
                Stop

            else
                let
                    updateRoom =
                        Dict.values nextPlayers
                            |> List.map (\player -> player.name)
                            |> Api.Room.Snapshot
                            |> encodeSnapshot
                            |> (\snapshot -> object [ ( "update", snapshot ) ])
                            |> Net.sendToAll (Dict.keys nextPlayers)
                in
                Keep ( nextModel, updateRoom )



-- SUBSCRIPTIONS


subscriptions : Model -> Sub Msg
subscriptions model =
    Net.disconnections Disconnect



-- ENCODE


encodeSnapshot : Api.Room.Snapshot -> Value
encodeSnapshot snapshot =
    object [ ( "players", list string snapshot.players ) ]
